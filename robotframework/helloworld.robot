*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Variables ***
${MESSAGE}       Hello, world!

*** Test Cases ***
Go to home page
    Wait Until Page Contains    Hello
Another Test
    Should Be Equal    ${MESSAGE}    Hello, world!



