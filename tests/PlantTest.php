<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Plant.php");

class PlantTest extends \PHPUnit\Framework\TestCase
{
   public function testgetPlantFlowering()
   {
       $plant = new plant();
       $result = $plant->getPlantFlowering("Spring");
       $expected = 'Summer';
       $this->assertTrue($result == $expected);
   }
}
